#Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.4.1] - 10-05-2019

- Fixed error in view pages

## [2.4.0] - 24-04-2019

- Added support for 3rd+ level menu
- Fixed issue with dual titles in page view
- Removed JS code related to Accordion component

## [2.3.1] - 20-03-2019

- Fixed hovering menu when scrolling

## [2.3.0] - 13-02-2019

- Removed unnecessary template files
- Changed menu expanding from clicking to hovering


## [2.2.2] - 05-02-2019

- Fixed preview cards under horizontal boxes not holding 100% of width when set to render using rendered entity.

## [2.2.2] - 05-02-2019

- Fixed issue with preview cards under horizontal boxes not holding 100% of width when set to render using rendered entity.

## [2.2.1] - 05-02-2019

- Added stylings for view header placed under content footer region

## [2.2.0] - 21-01-2019

- Fixed boxes not holding 100% in horizontal boxes.
- Fixed links in views under page display having big font size.
- Decreased margin between menu elements so that they they won't drop in the next line if there is not enough space.
- Added Feature for breaking slogan to the next line if there is not enough space
- Removed CSS ellipses from resources preview-cards
 


## [2.1.0] - 06-12-2018

- Fixed issue with non-aligned filters
- Fixed issue with overlapping column (Teaser List)
- Fixed issue with Horizontal Boxes height
- Fixed CERN loader rendering upside down
- Fixed filters support regardless number of filters (Teaser List)
- Fixed Image Gallery stretching
- Added custom classes for view block styling of teaser lists
- Added prettier borders to agenda views
- Added border around news images
- Removed weird spinning spans behind CDS images
